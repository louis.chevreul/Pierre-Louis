#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
music class
"""

class Musique:
    """
    classe qui definit une musique avec :
    -titre
    -album
    -artiste
    -genre
    -sousgenre
    -duree
    -format
    -polyphonie
    -chemin
    """


    def __init__(self, titre, album, artiste, genre, sousgenre, duree, format, polyphonie, chemin):
        self.titre = titre
        self.album = album
        self.artiste = artiste
        self.genre = genre
        self.sousgenre = sousgenre
        self.duree = duree
        self.format = format
        self.polyphonie = polyphonie
        self.chemin = chemin
