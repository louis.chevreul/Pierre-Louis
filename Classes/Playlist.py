#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 13:33:51 2017

@author: louis
"""

class Playlist():
    """
    classe qui definit une playlist
    """

    def __init__(self):
        self.music_list = []
        self.name = ""

    def add_pist(self, pist):
        """
        add pist to the music list
        """

        self.music_list.append(pist)

    def set_name(self, name):
        """
        set the name of the playlist
        """

        self.name = name

    def get_total_time(self):
        """
        return the total time of the playlist
        """

        time = 0
        for pist in self.music_list:
            time += pist.duree

        return time

    def get_pist_count(self):
        count = 0

        for pist in self.music_list:
            count += 1

        return count

    def M3U_save(self, path):
        path += ".m3u"
        save = open(path, "w")
        for pist in self.music_list:
            save.write(pist.chemin + "\n")
        save.close()

    def XSPF_save(self, path):
        path += ".xspf"
        save = open(path, "w")
        header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n<trackList>\n"
        footer = "</trackList>\n</playlist>"

        save.write(header)

        for pist in self.music_list:
            save.write("<track><location>" + pist.chemin + "</location></track>\n")

        save.write(footer)
        save.close()

